<?php
include "../dao/Database.php";

abstract class BaseDao
{
    private $nameTable;

    /**
     * Inset data to Table by row
     *
     * @param object $row
     * @return int
     */
    public function inset($row)
    {
        $database = Database::createDatabase();
        $database->insetTable($this->nameTable, $row);
        return 1;
    }

    /**
     * Update Table by row
     *
     * @param object $row
     * @return int
     */
    public function update($row)
    {
        $database = Database::createDatabase();
        $database->updateTable($this->nameTable, $row);
        return 1;
    }

    /**
     * Delete row in Table buy row
     *
     * @param object $row
     */
    public function delete($row)
    {
        $database = Database::createDatabase();
        $database->deleteTable($this->nameTable, $row);
    }

    /**
     * Find Table
     *
     * @return array Table
     */

    public function findAll()
    {
        $database = Database::createDatabase();
        if ($this->nameTable == $database::CATAGORY_TABLE)
            return $database->catagoryTable;
        if ($this->nameTable == $database::PRODUCT_TABLE)
            return $database->productTable;
        return $database->accessoryTable;
    }

    /**
     * Find Table by id
     *
     * @param int $id
     * @return array Table
     */
    public function findById($id)
    {
        $database = Database::createDatabase();
        if ($this->nameTable == $database::CATAGORY_TABLE) {
            $key = array_search($id, array_column($database->catagoryTable, 'id'));
            return $database->catagoryTable[$key];
        }
        if ($this->nameTable == $database::PRODUCT_TABLE) {
            $key = array_search($id, array_column($database->productTable, 'id'));
            return $database->productTable[$key];
        }
        $key = array_search($id, array_column($database->accessoryTable, 'id'));
        return $database->accessoryTable[$key];
    }

    /**
     * Set name Table by nameTable
     *
     * @param string $nameTable
     * @return  void
     */
    public function setNameTable($nameTable)
    {
        $this->nameTable = $nameTable;
    }
}
