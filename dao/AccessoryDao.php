<?php
include "BaseDao.php";

class AccessoryDao extends BaseDao
{
    function __construct()
    {
        $database = Database::createDatabase();
        parent::setNameTable($database::ACCESSORY_TABLE);
    }
}
