<?php

interface IDao
{
    public function inset($row);
    public function update($row);
    public function delete($row);
    public function findAll();
    public function findById($id);
    public function setNameTable($nameTable);
}
