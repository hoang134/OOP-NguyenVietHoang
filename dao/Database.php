<?php

class Database
{
    public $productTable = [];
    public $catagoryTable = [];
    public $accessoryTable = [];
    public $instant;
    const PRODUCT_TABLE = 'productTable';
    const CATAGORY_TABLE = 'catagoryTable';
    const ACCESSORY_TABLE = 'accssoryTable';
    const NAME_PRODUCT = 'Product';
    const NAME_CATAGORY = 'Category';
    const NAME_ACCESSORY = 'Accessotion';

    private static $singletonObj = null;

    private function __construct()
    {

    }

    /**
     * Only allow create one instance Database with class Database
     * @return Database
     */
    public static function createDatabase()
    {
        if (self::$singletonObj != null) {
            return self::$singletonObj;
        }
        self::$singletonObj = new self();
        return self::$singletonObj;
    }

    /**
     * Check parameter row
     *
     * @param array|Object $row
     * @return bool
     */
    private function isRowObject($row)
    {
        if (
            get_class($row) == self::NAME_ACCESSORY
            || get_class($row) == self::NAME_PRODUCT
            || get_class($row) == self::NAME_CATAGORY
        )
            return true;
        return false;
    }

    /**
     * Inset data to Table by name, row
     *
     * @param string $name
     * @param array|object $row
     * @return int
     */
    public function insetTable($name, $row)
    {
        if ($name == self::PRODUCT_TABLE) {
            array_push($this->productTable, $row);
            return 1;
        }
        if ($name == self::CATAGORY_TABLE) {
            array_push($this->catagoryTable, $row);
            return 1;
        }
        array_push($this->accessoryTable, $row);
        return 1;
    }

    /**
     * Get all data in array table by name
     *
     * @param string $name
     * @param string $where
     * @return array table
     */
    public function selectTable($name, $where)
    {
        if ($name == self::PRODUCT_TABLE)
            return $this->productTable;
        if ($name == self::CATAGORY_TABLE)
            return $this->catagoryTable;
        return $this->accessoryTable;
    }

    /**
     * Update data in array table by name, row
     *
     * @param string $name
     * @param array|object $row
     * @return int
     */
    public function updateTable($name, $row)
    {
        if ($name == self::PRODUCT_TABLE) {
            $key = array_search($row['id'], array_column($this->productTable, 'id'));
            if (!is_int($key))
                return 0;
            $this->productTable[$key] = $row;
            return 1;
        }
        if ($name == self::CATAGORY_TABLE) {
            $key = array_search($row['id'], array_column($this->catagoryTable, 'id'));
            if (!is_int($key))
                return 0;
            $this->catagoryTable[$key] = $row;
            return 1;
        }
        $key = array_search($row['id'], array_column($this->accessoryTable, 'id'));
        if (is_int($key))
            return 0;
        $this->accessoryTable = array_replace($this->accessoryTable, $row);
        return 1;
    }

    /**
     * Delete row in array table by name, row
     *
     * @param string $name
     * $@param array|object $row
     * @return bool
     */

    public function deleteTable($name, $row)
    {
        if ($name == self::PRODUCT_TABLE) {
            $key = array_search($row['id'], array_column($this->productTable, 'id'));
            if (!is_int($key))
                return false;
            unset($this->productTable[$key]);
            return true;
        }
        if ($name == self::CATAGORY_TABLE) {
            $key = array_search($row['id'], array_column($this->catagoryTable, 'id'));
            if (!is_int($key))
                return false;
            unset($this->catagoryTable[$key]);
            return true;
        }
        $key = array_search($row['id'], array_column($this->accessoryTable, 'id'));
        if (!is_int($key))
            return false;
        unset($this->accessoryTable[$key]);
        return true;
    }

    /**
     * Delete all data in array by name
     *
     * @param string $name
     * @return void
     */
    public function truncateTable($name)
    {
        if ($name == self::PRODUCT_TABLE) {
            unset($this->productTable);
            $this->productTable = [];
        }
        if ($name == self::CATAGORY_TABLE) {
            unset($this->catagoryTable);
            $this->catagoryTable = [];
        }
        unset($this->accessoryTable);
        $this->accessoryTable = [];
    }
}
