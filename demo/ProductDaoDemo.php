<?php
include "entity/Product.php";
include "dao/categoryDAO.php";
include "dao/Database.php";
class ProductDaoDemo {

    public function insetTest()
    {
        $category = new Product();
        $category->setname('ABC');
        $category->setId(1);
        $category->setCategoryId(10);
        $categoryDao = new CategoryDao();
        $categoryDao->inset($category);

        $database = Database::createDatabase();
        var_dump($database->catagoryTable);
    }

    public function updateTest()
    {
        $category = new Product();
        $category->setname('ABC');
        $category->setId(1);
        $category->setCategoryId(10);

        $categoryDao = new CategoryDao();
        $categoryDao->update($category);

        $database = Database::createDatabase();
        var_dump($database->catagoryTable);
    }

    public function deleteTest()
    {
        $category = new Product();
        $category->setname('ABC');
        $category->setId(1);

        $categoryDao = new CategoryDao();
        $categoryDao->delete($category);

        $database = Database::createDatabase();
        var_dump($database->catagoryTable);
    }

    public function findAllTest()
    {
        $categoryDao = new CategoryDao();
        var_dump($categoryDao->findAll());;
    }

    public function findByIdTest()
    {
        $categoryDao = new CategoryDao();
        var_dump($categoryDao->findById(1));
    }
}