<?php
include "../dao/Database.php";

class DatabaseDemo
{
   
    public function insetTableTest()
    {
        $databases = Database::createDatabase();
        $databases->insetTable('productTable', ['id' => 1, 'value' => 'ABC']);
        print_r($databases->productTable);
    }

    public function selectTableTest()
    {
        $databases = Database::createDatabase();
        print_r($databases->selectTable('productTable', 1));
    }

    public function updateTableTest()
    {
       $databases = Database::createDatabase();
       array_push($databases->productTable, ['id' => 1, 'value' => 'ABC']);
       if ($databases->updateTable('productTable', ['id' => 1, 'value' => 'JQK']) != 0) 
            print_r($databases->productTable);
        else 
            echo "Error";
        // $databaseDemo = new DatabaseDemo();
        // $databaseDemo->updateTableById('productTable',1,['id' => 1, 'value' => 'ABC']);
    }

    public function deleteTableTest()
    {
        $databases = Database::createDatabase();
        $databases->deleteTable('productTable', ['id' => 1, 'value' => 'ABC']);
        print_r($databases->productTable);
    }

    public function truncateTableTest()
    {
        $databases = Database::createDatabase();
        $databases->truncateTable('productTable');
    }

    public function initDatabase()
    {
        $databases = Database::createDatabase();
        for ($i = 0; $i < 10; $i++) {
            $databases->insetTable('productTable', ['id' => $i, 'value' => "JQK.$i"]);
        }
        var_dump($databases->productTable);
    }

    public function printTableTest()
    {
        $databases = Database::createDatabase();
        var_dump($databases->productTable);
        var_dump($databases->catagoryTable);
        var_dump($databases->accessoryTable);
    }

    public function updateTableById($name, $id, $row)
    {
        $databases = Database::createDatabase();
        if ($name == 'productTable')
        {
            $key = array_search($id,array_column($databases->accessoryTable,'id'));
            $databases->accessoryTable[$key] = $row;
        }

        elseif ($name == 'catagoryTable') {
            $key = array_search($id,array_column($databases->accessoryTable,'id'));
            $databases->accessoryTable[$key] = $row;
        }
        else
        {
            $key = array_search($id,array_column($databases->accessoryTable,'id'));
            $databases->accessoryTable[$key] = $row;
        }
    }

}