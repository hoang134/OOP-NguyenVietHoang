<?php
include "../entity/Product.php";
include "../entity/Category.php";

class  ProductDemo
{

    public function createProductTest()
    {
        $product = new Product();
        $product->setId(1);
        $product->setName('XYZ');
        $product->setCategoryId(10);
        return $product;
    }

    public function printProduct(Product $product)
    {
//        $product = new Product();
//        $product->getId();
        print("tên sản phẩm :" . $product->getName());
        print("id sản phẩm :" . $product->getId());
        print("categoryId sản phẩm :" . $product->getCategoryId());
    }

    public function createCategoryTest()
    {
        $category = new Category();
        $category->setId(100);
        $category->setname('XYZ');
        return $category;
    }

    public function printCategory(Category $category)
    {
        print("tên category : $category->getName()");
        print("id category : $category->getId()");
    }
}