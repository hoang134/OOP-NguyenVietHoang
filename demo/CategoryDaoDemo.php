<?php
include "../dao/CategoryDAO.php";
include "../entity/Category.php";


class CategoryDaoDemo
{
    public function insetTest()
    {
        $category = new Category();
        $category->setname('ABC');
        $category->setId(1);
        $categoryDao = new CategoryDao();
        var_dump($categoryDao->inset($category));
    }

    public function updateTest()
    {
        $category = new Category();
        $category->setname('ABC');
        $category->setId(1);

        $categoryDao = new CategoryDao();
        $categoryDao->update($category);
        var_dump($categoryDao->update($category));
    }

    public function deleteTest()
    {
        $category = new Category();
        $category->setname('ABC');
        $category->setId(1);

        $categoryDao = new CategoryDao();
        $categoryDao->delete($category);

        $database = Database::createDatabase();
        var_dump($database->catagoryTable);
    }

    public function findAllTest()
    {
        $categoryDao = new CategoryDao();
        var_dump($categoryDao->findAll());;
    }

    public function findByIdTest()
    {
        $categoryDao = new CategoryDao();
        var_dump($categoryDao->findById(1));
    }

}