<?php
include "BaseRow.php";
include "IEnity.php";
class Product extends BaseRow implements IEnity
{

    private $categoryId;

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }
}
