<?php
interface IEnity
{
    public function getName();
    public function setName($name);
    public function getId();
    public function setId($id);
}
